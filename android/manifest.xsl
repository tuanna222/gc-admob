<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:android="http://schemas.android.com/apk/res/android">

	<xsl:param name="admobBannerTopId" />
	<xsl:param name="admobBannerBottomId" />
	<xsl:param name="admobInterstitialId" />
	<xsl:param name="admobBannerId" />

	<xsl:template match="meta-data[@android:name='ADMOB_BANNER_TOP_ID']">
		<meta-data android:name="ADMOB_BANNER_TOP_ID" android:value="{$admobBannerTopId}"/>
	</xsl:template>
	<xsl:template match="meta-data[@android:name='ADMOB_BANNER_BOTTOM_ID']">
		<meta-data android:name="ADMOB_BANNER_BOTTOM_ID" android:value="{$admobBannerBottomId}"/>
	</xsl:template>
	<xsl:template match="meta-data[@android:name='ADMOB_INTERSTITIAL_ID']">
		<meta-data android:name="ADMOB_INTERSTITIAL_ID" android:value="{$admobInterstitialId}"/>
	</xsl:template>
	<xsl:template match="meta-data[@android:name='ADMOB_BANNER_ID']">
		<meta-data android:name="ADMOB_BANNER_ID" android:value="{$admobBannerId}"/>
	</xsl:template>

	<xsl:output indent="yes" />
	<xsl:template match="comment()" />

	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>
