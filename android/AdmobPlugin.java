package com.tealeaf.plugin.plugins;
import java.util.Map;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import com.tealeaf.EventQueue;
import com.tealeaf.TeaLeaf;
import com.tealeaf.logger;
import android.content.pm.PackageManager;
import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import java.util.HashMap;

import com.tealeaf.plugin.IPlugin;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Intent;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.widget.FrameLayout;
import android.view.Gravity;
import android.view.View;


import com.tealeaf.EventQueue;
import com.tealeaf.event.*;

import com.google.android.gms.ads.*;

public class AdmobPlugin implements IPlugin {
	//new
	private AdView adView;
	private boolean adLoaded = false;
	//old
	private AdView adTopView;
	private AdView adBottomView;
	private InterstitialAd adInterstitial;
	private boolean adTopLoaded = false;
	private boolean adBottomLoaded = false;

	public AdmobPlugin() {
	}

	public void onCreateApplication(Context applicationContext) {
	}

	public void onCreate(Activity activity, Bundle savedInstanceState) {
        PackageManager manager = activity.getPackageManager();
        String admobTopID = "";
        String admobBottomID = "";
        String admobInterstitialID = "";
        try {
            Bundle meta = manager.getApplicationInfo(activity.getPackageName(), PackageManager.GET_META_DATA).metaData;
            if (meta != null) {
            	//old
                admobTopID = meta.getString("ADMOB_BANNER_TOP_ID");
                admobBottomID = meta.getString("ADMOB_BANNER_BOTTOM_ID");
                admobInterstitialID = meta.getString("ADMOB_INTERSTITIAL_ID");
                if(!admobTopID.trim().equals("")){
                	adTopView = new AdView(activity);
                	adTopView.setAdSize(AdSize.BANNER);
					adTopView.setAdUnitId(admobTopID);
				}
                if(!admobBottomID.trim().equals("")){
                	adBottomView = new AdView(activity);
                	adBottomView.setAdSize(AdSize.BANNER);
					adBottomView.setAdUnitId(admobBottomID);
                }
                if(!admobInterstitialID.trim().equals("")){
                	adInterstitial = new InterstitialAd(activity);
                	adInterstitial.setAdUnitId(admobInterstitialID);
                }

                //new
                String adID = meta.getString("ADMOB_BANNER_ID");
                if(!adID.trim().equals("")){
                	adView = new AdView(activity);
                	adView.setAdSize(AdSize.BANNER);
					adView.setAdUnitId(adID);
					// logger.log("{admob} Initializing from manifest with admobBannerID=", adID);
                }
            }
        } catch (Exception e) {
            android.util.Log.d("{admob} EXCEPTION", "" + e.getMessage());
            e.printStackTrace();
        }

		//logger.log("{admob} Initializing from manifest with admobTopID=", admobTopID);
		//logger.log("{admob} Initializing from manifest with admobBottomID=", admobBottomID);
		//logger.log("{admob} Initializing from manifest with admobInterstitialID=", admobInterstitialID);
	}

	public void initBanner(final String json) {
		logger.log("{admob} initBanner");
		if(adView != null){
			TeaLeaf.get().runOnUiThread(new Runnable()
			{
			    public void run()
			    {
			    	try {
			    		JSONObject obj = new JSONObject(json);
						final String pos = obj.getString("position");
						adView.setAdListener(new AdListener() {
						    public void onAdLoaded(){
						    	logger.log("{admob} ad loaded");
						    	if(adLoaded) return;
						    	FrameLayout group = TeaLeaf.get().getGroup();
						    	if(pos.equals("top")){
						    		final FrameLayout.LayoutParams adViewLayoutParams = new FrameLayout.LayoutParams(
							    				FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT, Gravity.TOP | Gravity.CENTER_HORIZONTAL);
										group.addView(adView, adViewLayoutParams);
								}else{
									final FrameLayout.LayoutParams adViewLayoutParams = new FrameLayout.LayoutParams(
							    				FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL);
										group.addView(adView, adViewLayoutParams);
								}
								adView.setVisibility(View.GONE);
								adView.bringToFront();
						        adLoaded = true;
						    }
						});
						adView.loadAd(new AdRequest.Builder()
							// .addTestDevice("D7B1A13F6D29C55FBFAE9CAADED614D3")
							.build());
					} catch(Exception e) {
						logger.log("{admob} Exception while Initializing ads:", e.getMessage());
					}
			    }
			});
		}
	}

	public void showBanner(final String json) {
		if(adView != null){
			TeaLeaf.get().runOnUiThread(new Runnable()
			{
			    public void run()
			    {
					logger.log("{admob} loadAd...");
					if(adLoaded){
						adView.setVisibility(View.VISIBLE);
					}
				}
			});
		}
	}

	public void hideBanner(final String json) {
		if(adView != null){
			TeaLeaf.get().runOnUiThread(new Runnable()
			{
			    public void run()
			    {
					logger.log("{admob} hideBanner");
					// adView.destroy();
					if(adLoaded){
						adView.setVisibility(View.GONE);
					}
				}
			});
		}
	}

    public void showTopBanner(final String json) {
	    if(adTopView != null){
	    	new Thread()
			{
			    public void run()
			    {
			        TeaLeaf.get().runOnUiThread(new Runnable()
			        {
			            public void run()
			            {
			                logger.log("{admob} showTopBanner");
						    adTopView.setAdListener(new AdListener() {
							    public void onAdLoaded(){
							    	logger.log("{admob} top ad loaded");
							    	if(adTopLoaded) return;
							    	FrameLayout group = TeaLeaf.get().getGroup();
					    			final FrameLayout.LayoutParams adViewLayoutParams = new FrameLayout.LayoutParams(
					    				FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT, Gravity.TOP | Gravity.CENTER_HORIZONTAL);
							        group.addView(adTopView, adViewLayoutParams);
							        adTopView.bringToFront();
							        adTopLoaded = true;
							    }
							});
							adTopView.loadAd(new AdRequest.Builder()
								// .addTestDevice("D7B1A13F6D29C55FBFAE9CAADED614D3")
								.build());
			            }
			        });
			    }
			}.start();
	    }
    }

    public void showBottomBanner(final String json) {
	    if(adBottomView != null){
	    	new Thread()
			{
			    public void run()
			    {
			        TeaLeaf.get().runOnUiThread(new Runnable()
			        {
			            public void run()
			            {
			                logger.log("{admob} showBottomBanner");
						    adBottomView.setAdListener(new AdListener() {
							    public void onAdLoaded(){
							    	logger.log("{admob} bottom ad loaded");
							    	if(adBottomLoaded) return;
							    	FrameLayout group = TeaLeaf.get().getGroup();
					    			final FrameLayout.LayoutParams adViewLayoutParams = new FrameLayout.LayoutParams(
					    				FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL);
							        group.addView(adBottomView, adViewLayoutParams);
							        adBottomView.bringToFront();
							        adBottomLoaded = true;
							    }
							});
							adBottomView.loadAd(new AdRequest.Builder().build());
			            }
			        });
			    }
			}.start();
	    }
    }

    public void showInterstitial(final String json){
    	if(adInterstitial != null){
	    	new Thread()
			{
			    public void run()
			    {
			        TeaLeaf.get().runOnUiThread(new Runnable()
			        {
			            public void run()
			            {
			                logger.log("{admob} showInterstitial");
						    adInterstitial.setAdListener(new AdListener() {
							    public void onAdLoaded(){
							    	logger.log("{admob} adInterstitial loaded");
							    	if (adInterstitial.isLoaded()) {
								      	adInterstitial.show();
								    }
							    }
							});
							adInterstitial.loadAd(new AdRequest.Builder()
								// .addTestDevice("D7B1A13F6D29C55FBFAE9CAADED614D3")
								.build());
			            }
			        });
			    }
			}.start();
	    }
    }

	public void onResume() {
	}

	public void onStart() {
	}

	public void onPause() {
	}

	public void onStop() {
	}

	public void onDestroy() {
	}

	public void onNewIntent(Intent intent) {
	}

	public void setInstallReferrer(String referrer) {
	}

	public void onActivityResult(Integer request, Integer result, Intent data) {
	}

	public boolean consumeOnBackPressed() {
		return true;
	}

	public void onBackPressed() {
	}
}

