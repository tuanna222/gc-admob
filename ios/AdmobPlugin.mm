#import "AdmobPlugin.h"

@implementation AdmobPlugin
// The plugin must call super dealloc.
- (void) dealloc {
	self.interstitial_ = nil;
	self.bannerTop_ = nil;
    self.bannerBot_ = nil;
    self.adBannerView = nil;
    self.admobInterstitialId = @"";
    self.adBannerLoaded = FALSE;
	[super dealloc];
}

// The plugin must call super init.
- (id) init {
	self = [super init];
	if (!self) {
		return nil;
	}
	self.interstitial_ = nil;
	self.bannerTop_ = nil;
    self.bannerBot_ = nil;
    self.adBannerView = nil;
    self.admobInterstitialId = @"";
    self.adBannerLoaded = FALSE;
	return self;
}

- (void) initializeWithManifest:(NSDictionary *)manifest appDelegate:(TeaLeafAppDelegate *)appDelegate {
	@try {
		NSLog(@"{admob} Initialized with manifest");
		self.tealeafViewController_ = appDelegate.tealeafViewController;
		NSDictionary *ios = [manifest valueForKey:@"ios"];
		NSString *admobInterstitialId = [ios valueForKey:@"admobInterstitialId"];
        self.admobInterstitialId = admobInterstitialId;
        [self preLoadInterstitial];
        //banner
        float frameWidth = self.tealeafViewController_.view.frame.size.width;
        float frameHeight = self.tealeafViewController_.view.frame.size.height;

        NSString *admobBannerTopId = [ios valueForKey:@"admobBannerTopId"];
        GADAdSize adSize = [self adSizeForOrientation:self.tealeafViewController_.interfaceOrientation];
        if (admobBannerTopId && ![admobBannerTopId isEqualToString:@""]) {
            self.bannerTop_ = [[GADBannerView alloc] initWithAdSize:adSize];
            self.bannerTop_.adUnitID = admobBannerTopId;
            self.bannerTop_.rootViewController = self.tealeafViewController_;
            [self.tealeafViewController_.view addSubview:self.bannerTop_];
            self.bannerTop_.frame = CGRectMake(
                frameWidth/2.0 - self.bannerTop_.frame.size.width/2.0, 0,
                self.bannerTop_.frame.size.width, self.bannerTop_.frame.size.height);
        }
        NSString *admobBannerBottomId = [ios valueForKey:@"admobBannerBottomId"];
        if (admobBannerBottomId && ![admobBannerBottomId isEqualToString:@""]) {
            self.bannerBot_ = [[GADBannerView alloc] initWithAdSize:adSize];
            self.bannerBot_.adUnitID = admobBannerBottomId;
            self.bannerBot_.rootViewController = self.tealeafViewController_;
            [self.tealeafViewController_.view addSubview:self.bannerBot_];
            self.bannerBot_.frame = CGRectMake(frameWidth/2.0 - self.bannerBot_.frame.size.width/2.0,
                                               frameHeight - self.bannerBot_.frame.size.height,
                                           self.bannerBot_.frame.size.width,self.bannerBot_.frame.size.height);
        }
        
        NSString *admobBannerId = [ios valueForKey:@"admobBannerId"];
        if (admobBannerId && ![admobBannerId isEqualToString:@""]) {
            self.adBannerView = [[GADBannerView alloc] initWithAdSize:adSize];
            self.adBannerView.adUnitID = admobBannerId;
            self.adBannerView.delegate = self;
        }
        
	}
	@catch (NSException *exception) {
		NSLog(@"{admob} Failure to get ios:admobId from manifest file: %@", exception);
	}
}

- (void)preLoadInterstitial {
    if (self.admobInterstitialId && ![self.admobInterstitialId isEqualToString:@""]) {
        NSLog(@"{admob} interstitial ads");
        self.interstitial_ = [[GADInterstitial alloc] init];
        self.interstitial_.adUnitID = self.admobInterstitialId;
        GADRequest *request = [GADRequest request];
//        request.testDevices = @[ GAD_SIMULATOR_ID ];
        [self.interstitial_ loadRequest:request];
    }
}

- (void) showInterstitial:(NSDictionary *)jsonObject {
	@try {
		NSLog(@"{admob} Showing interstitial");
        [self.interstitial_ presentFromRootViewController:self.tealeafViewController_];
        [NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector:@selector(preLoadInterstitial) userInfo:nil repeats:NO];
	}
	@catch (NSException *exception) {
		NSLog(@"{admob} Failure during interstitial: %@", exception);
	}
}

- (void) initBanner:(NSDictionary *)jsonObject {
	@try {
        if(self.adBannerView == nil){
            return;
        }
        NSString *pos = [jsonObject valueForKey:@"position"];
        float frameWidth = self.tealeafViewController_.view.frame.size.width;
        float frameHeight = self.tealeafViewController_.view.frame.size.height;
        self.adBannerView.rootViewController = self.tealeafViewController_;
        [self.tealeafViewController_.view addSubview: self.adBannerView];
        if([pos isEqualToString:@"top"]){
            self.adBannerView.frame = CGRectMake(
                                               frameWidth/2.0 - self.adBannerView.frame.size.width/2.0, 0,
                                               self.adBannerView.frame.size.width, self.adBannerView.frame.size.height);
        }else{
            self.adBannerView.frame = CGRectMake(frameWidth/2.0 - self.adBannerView.frame.size.width/2.0,
                                               frameHeight - self.adBannerView.frame.size.height,
                                               self.adBannerView.frame.size.width, self.adBannerView.frame.size.height);
        }
//        [self.adBannerView setHidden: TRUE];
        self.adBannerView.hidden = YES;
        GADRequest *request = [GADRequest request];
        //            request.testDevices = @[ GAD_SIMULATOR_ID ];
//        request.testDevices = @[ @"3ba4a4be584ece7d9eee60a0ba3ea262" ];
        [self.adBannerView loadRequest:request];
	}
	@catch (NSException *exception) {
		NSLog(@"{admob} Failure during init banner: %@", exception);
	}
}

- (void)adViewDidReceiveAd:(GADBannerView *)adView {
//    NSLog(@"adViewDidReceiveAd");
    self.adBannerLoaded = TRUE;
}

/// Called when an ad request failed.
- (void)adView:(GADBannerView *)adView didFailToReceiveAdWithError:(GADRequestError *)error {
//    NSLog(@"adViewDidFailToReceiveAdWithError: %@", [error localizedDescription]);
    self.adBannerLoaded = FALSE;
}

- (void) showBanner:(NSDictionary *)jsonObject {
    if (self.adBannerLoaded == TRUE) {
//        NSLog(@"showBanner");
//        [self.adBannerView setHidden: FALSE];
        self.adBannerView.hidden = NO;
    }
}

- (void) hideBanner:(NSDictionary *)jsonObject {
    if (self.adBannerLoaded == TRUE) {
//        NSLog(@"hideBanner");
//        [self.adBannerView setHidden: TRUE];
        self.adBannerView.hidden = YES;
    }
}


- (void) showTopBanner:(NSDictionary *)jsonObject {
    @try {
        if(self.bannerTop_){
//            NSLog(@"{admob} Showing banner top");
            GADRequest *request = [GADRequest request];
//            request.testDevices = @[ GAD_SIMULATOR_ID ];
            [self.bannerTop_ loadRequest:request];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"{admob} Failure during banner: %@", exception);
    }
}

- (void) showBottomBanner:(NSDictionary *)jsonObject {
    @try {
        if(self.bannerBot_){
//            NSLog(@"{admob} Showing banner bottom");
            [self.bannerBot_ loadRequest:[GADRequest request]];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"{admob} Failure during banner: %@", exception);
    }
}

- (GADAdSize)adSizeForOrientation:(UIInterfaceOrientation)orientation {
    // Landscape.
    // Only some networks support a thin landscape size
    // (480x32 on iPhone or 1024x90 on iPad).
    //if (UIInterfaceOrientationIsLandscape(orientation)) {
    //    return kGADAdSizeSmartBannerLandscape;
    //}
    // Portrait.
    // Most networks support banner (320x50) and Leaderboard (728x90)
    // sizes.
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return kGADAdSizeLeaderboard;
    } else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        return kGADAdSizeBanner;
    }
    // Unknown idiom.
    return kGADAdSizeBanner;
}
@end

