#import "PluginManager.h"
#import <GoogleMobileAds/GoogleMobileAds.h>

@interface AdmobPlugin : GCPlugin <GADBannerViewDelegate>

@property (nonatomic, retain) TeaLeafViewController *tealeafViewController_;
@property (nonatomic, retain) GADInterstitial *interstitial_;
@property (nonatomic, retain) GADBannerView *bannerTop_;
@property (nonatomic, retain) GADBannerView *bannerBot_;
@property (nonatomic, retain) GADBannerView *adBannerView;
@property (nonatomic, retain) NSString *admobInterstitialId;
@property (nonatomic, assign) BOOL adBannerLoaded;
@end
