var hasNativeEvents = NATIVE && NATIVE.plugins && NATIVE.plugins.sendRequest;

var admob = Class(function () {
	this.init = function () {
	}

	this.showInterstitial = function () {
		logger.log("{admob} Showing interstitial");
		if (NATIVE && NATIVE.plugins && NATIVE.plugins.sendEvent) {
			logger.log("sendEvent AdmobPlugin...");
			NATIVE.plugins.sendEvent("AdmobPlugin", "showInterstitial",
				JSON.stringify({}));
		}
	};

	this.initBanner = function (position) {
		if (NATIVE && NATIVE.plugins && NATIVE.plugins.sendEvent) {
			NATIVE.plugins.sendEvent("AdmobPlugin", "initBanner",
				JSON.stringify({position: position}));
		}
	};

	this.showBanner = function () {
		if (NATIVE && NATIVE.plugins && NATIVE.plugins.sendEvent) {
			NATIVE.plugins.sendEvent("AdmobPlugin", "showBanner",
				JSON.stringify({}));
		}
	};

	this.hideBanner = function () {
		if (NATIVE && NATIVE.plugins && NATIVE.plugins.sendEvent) {
			NATIVE.plugins.sendEvent("AdmobPlugin", "hideBanner",
				JSON.stringify({}));
		}
	};

	this.showTopBanner = function () {
		logger.log("{admob} Showing top banner");
		if (NATIVE && NATIVE.plugins && NATIVE.plugins.sendEvent) {
			logger.log("sendEvent AdmobPlugin...");
			NATIVE.plugins.sendEvent("AdmobPlugin", "showTopBanner",
				JSON.stringify({}));
		}
	};

	this.showBottomBanner = function () {
		logger.log("{admob} Showing bottom banner");
		if (NATIVE && NATIVE.plugins && NATIVE.plugins.sendEvent) {
			logger.log("sendEvent AdmobPlugin...");
			NATIVE.plugins.sendEvent("AdmobPlugin", "showBottomBanner",
				JSON.stringify({}));
		}
	};
});

exports = new admob();
